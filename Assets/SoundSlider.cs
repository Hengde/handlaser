﻿using UnityEngine;
using System.Collections;

public class SoundSlider : MonoBehaviour {

    public AudioClip[] acs = new AudioClip[7];
    AudioSource audioPlay;
    TouchHandler th;

	bool editing;

	// Use this for initialization
	void Start () {
        audioPlay = GetComponent<AudioSource>();
        th = FindObjectOfType<TouchHandler>();
	}
		

	void Update(){

		if(editing && Input.GetTouch(0).phase ==TouchPhase.Ended){
			editing = false;
			gameObject.SetActive(false);
		}
	}
	
	// Update is called once per frame
	public void ChangeSpot (float spot) {
        audioPlay.PlayOneShot(acs[(int)spot]);
        Debug.Log("played" + spot);
        th.currentSound = (int)spot;
		th.currentTB.ac = acs[(int)spot].name;
		string soundName;
		th.currentNodeSettings.music = acs[(int)spot].name;
		th.currentNodeSettings.spawnType = SpawnType.None;
		editing = true;
	}
}
