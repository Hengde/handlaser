﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MessengerScript : MonoBehaviour {
    TouchHandler th;

    // Use this for initialization
    void Start () {
        th = FindObjectOfType<TouchHandler>();
    }
	
	// Update is called once per frame
	public void SetMessage (string txt) {
        th.currentTB.text = txt;
		th.currentNodeSettings.message = txt;
		gameObject.SetActive(false);
        this.GetComponent<InputField>().text = "";
		th.currentNodeSettings.spawnType = SpawnType.None;
	}
}
