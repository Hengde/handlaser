﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CharacterMovement : MonoBehaviour {

	public float speed;
	public bool invertRotation;
	public bool invertMovement;
	public int rotateXDegrees;
	CharacterController controller;
	private bool rotationCoroutineStarted;
	private GameObject currentIceTile;
	// Use this for initialization

	float footPrintCounter; //how long til our next footprints
	public float footPrintDistance; //draw footprints every X amount

	void Start () {
		controller = GetComponent<CharacterController>();

		rotationCoroutineStarted = false;
		currentIceTile = null;
	}
	
	// Update is called once per frame
	void Update () {
			
	}

	public void move(bool forward) {
		if(currentIceTile != null){
			return;
		}
		if(invertMovement) {
			forward = !forward;
		}
		Vector3 moveDirection;
		if(forward) {
			moveDirection = transform.TransformDirection(Vector3.forward) * speed;
		}
		else {
			moveDirection = transform.TransformDirection(Vector3.forward) * speed * -1;
		}
		controller.Move(moveDirection);
		handleFootprints();
	}

	public void handleFootprints(){
		if(footPrintCounter >=0){
			footPrintCounter-= Time.deltaTime;
		} else {
			footPrintCounter = footPrintDistance;
			GameObject footprints = Instantiate(Resources.Load("Prefabs/Footprints"),transform.position, Quaternion.identity) as GameObject;
			footprints.transform.eulerAngles = new Vector3(90,transform.eulerAngles.y,0);
			footprints.transform.position = new Vector3(transform.position.x, -3.09f, transform.position.z);

		}
			
	}

	public void rotateDegrees(float angle) {
		if(currentIceTile != null){
			return;
		}
		if(invertRotation) {
			angle *= -1;
		}
		if(rotateXDegrees != 0 && !rotationCoroutineStarted){
			float rotation = angle < 0 ? -rotateXDegrees : rotateXDegrees;
			StartCoroutine(segmentRotation(new Vector3(0, rotation, 0), 1));
		}
		else if(rotateXDegrees == 0){
			transform.Rotate(new Vector3(0, angle*5, 0));
		}
	}

	IEnumerator segmentRotation(Vector3 byAngles, float inTime) {
		var fromAngle = transform.rotation;
		var toAngle = Quaternion.Euler(transform.eulerAngles + byAngles);
		for (var t = 0f; t < 1; t += Time.deltaTime/inTime) {
			transform.rotation = Quaternion.Lerp(fromAngle, toAngle, t);
			yield return null;
		}
	}

	IEnumerator iceSlide() {
		while(currentIceTile != null){
			controller.Move(controller.velocity * Time.deltaTime);
			yield return null;
		}
	}

	public void startSliding(GameObject iceTile) {
		Vector3 newRotation = transform.eulerAngles;
		newRotation.y = (Mathf.Round(newRotation.y / 90) * 90);
		transform.eulerAngles = newRotation;
		//StartCoroutine(segmentRotation(newRotation, 0.1f));
		move(true);
		if(currentIceTile == null) {
			currentIceTile = iceTile;
			StartCoroutine(iceSlide());
		}
		else {
			currentIceTile = iceTile;
		}
	}

	public void stopSliding(GameObject iceTile) {
		if(currentIceTile == iceTile) {
			currentIceTile = null;
		}
		else if(iceTile == null) {
			currentIceTile = null;
		}
	}

	public void resetPosition(Vector3 startPoint) {
		transform.position = new Vector3(startPoint.x, transform.position.y, startPoint.z);
	}
}
