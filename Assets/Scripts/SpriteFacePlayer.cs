﻿using UnityEngine;
using System.Collections;

public class SpriteFacePlayer : MonoBehaviour {

	private Vector3 facing;
	Transform player;

	// Use this for initialization
	void Start () {
		player = GameObject.Find("Player").transform;
	}
	
	// Update is called once per frame
	void Update () {
		
		facing = new Vector3( player.position.x, 
								this.transform.position.y, 
								player.position.z ) ;
		this.transform.LookAt( facing ) ;

	}
}
