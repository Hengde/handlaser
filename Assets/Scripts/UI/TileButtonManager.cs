﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TileButtonManager : MonoBehaviour {

	public GameObject [] tileButtons;
	public GameObject activeButton;
	// Use this for initialization


	public void setActiveButton(GameObject tb){
//		Debug.Log("set "+tb);
		//i.color = new Color (1f,1f,1f,.5f);
		activeButton = tb;
		tb.transform.FindChild("Highlight").gameObject.SetActive(true);
		foreach(GameObject b in tileButtons){
			if(b != tb){
//				Debug.Log(b.transform.FindChild("Highlight"));
//				Debug.Log(b);
				b.transform.GetChild(1).gameObject.SetActive(false);
			}
		}
	}
}
