﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class DropdownScript : MonoBehaviour {

    public Text menuText;
    public Button notClicker;
    public List<TileButton> tbs = new List<TileButton>();
    float expandedHeight = 200;
    float expandedY;
    float smallHeight = 32;
    float smallY;
    RectTransform rt;

    public GameObject panel;

    void Start() {
        panel.SetActive(false);
    }

    public void Clicked(bool state) {
        if (state == true)
        {
            panel.SetActive(true);
            menuText.text = "Fewer Options";
        }
        else {
            panel.SetActive(false);
            menuText.text = "More Options";
        }
    }

	/*// Use this for initialization
	void Awake () {
        rt = GetComponent<RectTransform>();
        expandedY = rt.localPosition.y - (expandedHeight - smallHeight)/2;
        smallY = rt.localPosition.y;
        TileButton[] tempRBs = GetComponentsInChildren<TileButton>();
        foreach (TileButton tb in tempRBs)
        {
            tbs.Add(tb);
            tb.gameObject.SetActive(false);
        }
    }

    public void Clicked(bool state) {
        if (state == true)
        {
            menuText.enabled = false;
            notClicker.gameObject.SetActive(true);
            foreach (TileButton tb in tbs) {
                tb.gameObject.SetActive(true);
            }
            rt.sizeDelta = new Vector2 (rt.sizeDelta.x, expandedHeight);
            rt.localPosition = new Vector3(rt.localPosition.x, expandedY, rt.localPosition.z);
        }
        else {
            menuText.enabled = true;
            notClicker.gameObject.SetActive(false);
            foreach (TileButton tb in tbs)
            {
                tb.gameObject.SetActive(false);
            }
            rt.sizeDelta = new Vector2(rt.sizeDelta.x, smallHeight);
            rt.localPosition = new Vector3(rt.localPosition.x, smallY, rt.localPosition.z);
        }
    }*/

}
