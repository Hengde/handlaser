﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class TileButton : MonoBehaviour, ISelectHandler {

    public int tile;
    TouchHandler th;
    public MovementType mt = MovementType.Normal;
    public SpawnType st = SpawnType.None;
    public string text = "";
    public string ac = "";
    public EditButton[] edits = new EditButton[9];
    public Image facing;
    bool clickt;
    float clickLength;
    public float clickTime = .1f;
    bool editing = false;
    public InputField infield;
    public Slider slider;

	public TileButtonManager tileButtonManager;

	[SerializeField]
	NodeInfo currentNodeSetting;

    public bool editable;

    float maxtolerance = 200;
    float mintolerance = 40; //these values need to be tested

	void Awake() {
		currentNodeSetting = new NodeInfo();
		currentNodeSetting.movementAttribute = mt;
		currentNodeSetting.spawnType = st;
		currentNodeSetting.message = "";
		currentNodeSetting.music = "";

		//		Debug.Log(mt);
		//		Debug.Log(st);
	}

	void Start () {
		th = FindObjectOfType<TouchHandler>();
		transform.GetChild(1).gameObject.SetActive(false);
		tileButtonManager = GameObject.Find("UI").GetComponent<TileButtonManager>();
	}


    void Update ()
    {
        if (clickt == true) {
//			Debug.Log("I HAVE BEEN TOUCHED.");
            if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended)
            {
				//TODO: Shw that this one is active. Tell all others to not be active;

				tileButtonManager.setActiveButton(gameObject);
//				Debug.Log("got a click");
                Vector3 pos = Input.touches[0].position;
//				Debug.Log(Vector3.Distance(pos,transform.position));
                //if you're editing AND dragged the pointer a little way
				if (editing == true && (Vector3.Distance(transform.position, pos) <= maxtolerance) && (Vector3.Distance(transform.position, pos) >= mintolerance))
                {
//					Debug.Log("set to a new thing");
					//float ang = Vector3.Angle(pos, Vector3.right);
					float ang =  Mathf.Atan2(pos.y-transform.position.y, pos.x-transform.position.x)*180 / Mathf.PI;
//					Debug.Log(ang);
                    //all of these angles need to be tested
                    if (ang >= -10 && ang < 30) {
                        ChangeType(3);
                    }
                    else if (ang >= 30 && ang < 70)
                    {
                        ChangeType(8);
                    }
                    else if (ang >= 70 && ang < 110)
                    {
                        ChangeType(0);
                    }
                    else if (ang >= 110 && ang < 150)
                    {
                        ChangeType(7);
                    }
                    else if (ang >= 150 && ang < 180)
                    {
                        ChangeType(6);
                    }
                    else if (ang >= -180 && ang < -140)
                    {
                        ChangeType(5);
                    }
                    else if (ang >= -140 && ang < -100)
                    {
                        ChangeType(1);
                    }
                    else if (ang >= -100 && ang < -60)
                    {
                        ChangeType(2);
                    }
                    else if (ang >= -60 && ang < -10)
                    {
                        ChangeType(4);
                    }
                }
                else {
				//	Debug.Log("clicked in place"+currentNodeSetting.spawnType);
					th.setNode(currentNodeSetting);
//                    RaycastHit hit;
//                    var ray = Camera.main.ScreenPointToRay(pos);
//                    if (Physics.Raycast(ray, out hit) && hit.collider.gameObject.name != "Node")
//                    {
//                        Debug.Log("Make Node");
//                        GameObject newNode = (GameObject)Instantiate(Resources.Load("Prefabs/Node"), new Vector3(Mathf.Floor(hit.point.x), -3.6f, Mathf.Floor(hit.point.z)), Quaternion.identity);
//						newNode.GetComponent<NodeScript>().Initialize(mt, st, text, ac);
//
//                    }
                }

                clickt = false;
                clickLength = 0;
                foreach (EditButton eb in edits)
                {
                    eb.EndEdit();
                }
                //somehow set default placement type to be this button's stats
                th.currentTB = this;
            }
            else 
            {
                clickLength += Time.deltaTime;
                if (clickLength >= clickTime) {
                    foreach (EditButton eb in edits)
                    {
                        eb.StartEdit();
                    }
                    editing = true;
                }
            }
        }
        else if (Input.touchCount > 0 && Input.GetTouch(0).phase != TouchPhase.Ended) {
//			Debug.Log("there's a touch");
            Vector3 pos = Input.touches[0].position;
			//pos.x +=660;
            if ((Vector3.Distance(transform.position, pos) <= mintolerance)) {
				//Debug.Log("and its clicking");
                clickt = true;

            }
			else {
//			
			}
        }
    }


    public void OnSelect(BaseEventData bed) {
//	Debug.Log("Getting clicked");
        clickt = true;
    }

    void ChangeType(int mode) {
		//Debug.Log("changing type");
        facing.sprite = edits[mode].GetComponent<Image>().sprite;
        switch (mode) {
            case 0:
			
				currentNodeSetting.movementAttribute = MovementType.Ignore;
				currentNodeSetting.spawnType = SpawnType.Goal;
				currentNodeSetting.message = "";
				currentNodeSetting.music = "";
                break;
            case 1:
                slider.gameObject.SetActive(true);
				currentNodeSetting.movementAttribute = MovementType.Ignore;
				currentNodeSetting.spawnType = SpawnType.Ignore;
				currentNodeSetting.message = "";
                break;
            case 2:
                infield.gameObject.SetActive(true);
				currentNodeSetting.movementAttribute = MovementType.Ignore;
				currentNodeSetting.spawnType = SpawnType.Ignore;
				currentNodeSetting.music = "";
                break;
            case 3:
               
				currentNodeSetting.movementAttribute = MovementType.Ignore;
				currentNodeSetting.spawnType = SpawnType.Key;
				currentNodeSetting.message = "";
				currentNodeSetting.music = "";
                break;
            case 4:
                
				currentNodeSetting.movementAttribute = MovementType.Ignore;
				currentNodeSetting.spawnType = SpawnType.Lock;
				currentNodeSetting.message = "";
				currentNodeSetting.music = "";
                break;
            case 5:
				currentNodeSetting.movementAttribute = MovementType.Wall;
				currentNodeSetting.spawnType = SpawnType.None;
				currentNodeSetting.message = "";
				currentNodeSetting.music = "";
                break;
            case 6:
               
				currentNodeSetting.movementAttribute = MovementType.Normal;
				currentNodeSetting.spawnType = SpawnType.Ignore;
				currentNodeSetting.message = "";
				currentNodeSetting.music = "";
                break;
            case 7:
				currentNodeSetting.movementAttribute = MovementType.Ice;
				currentNodeSetting.spawnType = SpawnType.Ignore;
				currentNodeSetting.message = "";
				currentNodeSetting.music = "";
                break;
            case 8:
				currentNodeSetting.movementAttribute = MovementType.Ignore;
				currentNodeSetting.spawnType = SpawnType.StartPoint;
				currentNodeSetting.message = "";
				currentNodeSetting.music = "";
                break;

        }
		//Debug.Log("sending "+currentNodeSetting.spawnType);
		th.currentNodeSettings = currentNodeSetting;

    }



}

