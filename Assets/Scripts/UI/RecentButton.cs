﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RecentButton : MonoBehaviour {

    public int currentTile;
    public Image currentSprite;
    public Toggle dropdown;
    ButtonManager bm;
    

	// Use this for initialization
	void Awake () {
        bm = GetComponentInParent<ButtonManager>();
	}
	
	// Update is called once per frame
	public void selectTile () {
        bm.useTile(currentTile);
        dropdown.isOn = false;
	}
}
