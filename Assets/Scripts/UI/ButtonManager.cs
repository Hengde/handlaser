﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ButtonManager : MonoBehaviour {

    List<int> usedTiles = new List<int>();
    List<RecentButton> rbs = new List<RecentButton>();
    public List<Sprite> sprites = new List<Sprite>();
    public TouchHandler th;
    public InputField infield;
    public Slider slider;
    Vector2[] buttonsPos = new Vector2[9];


    void Awake () {

        foreach (Vector2 pos in buttonsPos) {
            //make a damn button for the position
            //set the button's value
        }
       /* RecentButton[] tempRBs = GetComponentsInChildren<RecentButton>();
        foreach (RecentButton rb in tempRBs) {
            rbs.Add(rb);
            usedTiles.Add(rb.currentTile);*/
        //}
        

	}

    void Start() {
    }
	



    public void useTile(int selectedTile) {


            switch (selectedTile)
            {
                case 0:
                    th.PlacementMode = PlacementType.Floor;
                    break;
                case 1:
                    th.PlacementMode = PlacementType.Wall;
                    break;
                case 2:
                    th.PlacementMode = PlacementType.Key;
                    break;
                case 3:
					th.PlacementMode = PlacementType.Lock;
                    break;
                case 4:
				    th.PlacementMode = PlacementType.Ice;
                    break;
                case 5:
			TouchScreenKeyboard.Open(infield.text);
                infield.gameObject.SetActive(true);
                Debug.Log("message button pressed");
	//			Debug.Log(TouchScreenKeyboard.isSupported);
                    //th.PlacementMode = PlacementType.Message;
                    break;
                case 6:
                slider.gameObject.SetActive(true);
				    //th.PlacementMode = PlacementType.AMajor;
                    break;
                case 7:
                    th.PlacementMode = PlacementType.Goal;
                    break;
                case 8:
                    th.PlacementMode = PlacementType.StartPoint;
                    break;
            case 9:
                th.PlacementMode = PlacementType.Camera;
                break;
            }
			Debug.Log(th.PlacementMode);
        }

    public void SetMessageTile(string msg)
    {
        th.currentText = msg;
        infield.gameObject.SetActive(false);
        th.PlacementMode = PlacementType.Message;
    }

    public void SetSoundTile()
    {
        slider.gameObject.SetActive(false);
        th.PlacementMode = PlacementType.AMajor;
    }

}

