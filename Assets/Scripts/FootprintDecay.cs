﻿using UnityEngine;
using System.Collections;

public class FootprintDecay : MonoBehaviour {


	public float timeTilFade;
	public float fadeLength;

	private float myLifeCounter;
	private bool fade;

	private float colorAlpha;
	private SpriteRenderer sr;

	// Use this for initialization
	void Start () {
		myLifeCounter = 0;
		colorAlpha = 1;
		fade = false;
		sr = GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
		if(!fade){
			if(myLifeCounter >= timeTilFade){
				fade = true;
				myLifeCounter = 0;
			}
		} else {
			colorAlpha = Mathf.Lerp(1,0,myLifeCounter/fadeLength);
			sr.color = new Color(sr.color.r, sr.color.g, sr.color.b, colorAlpha);
			if(myLifeCounter >= fadeLength){
				Destroy(this.gameObject);
			}
		}
		myLifeCounter +=Time.deltaTime;
	}
}
