﻿using UnityEngine;
using System.Collections;

public class PlayerStats : MonoBehaviour {

	private int keys;
	// Use this for initialization
	void Start () {
		keys = 0;	
	}

	public bool hasKey() {
		if(keys > 0){
			keys--;
			Debug.Log("You've got a key");
			return true;
		}
		Debug.Log("you don't have a key :(");
		return false;
	}

	public void getKey() {
		keys++;
		Debug.Log("Got Key");
	}

}
