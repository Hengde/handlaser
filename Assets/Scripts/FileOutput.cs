﻿using UnityEngine;
using System.Collections;
using System.IO;

public class FileOutput : MonoBehaviour {

	public GameObject Wall;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	}

	public void Save(int slotNum) {

		StreamWriter sw = new StreamWriter(getPath(slotNum));
		GameObject[] objects;
		objects = GameObject.FindGameObjectsWithTag("Node");
		for(int i = 0; i < objects.Length; i++) {
			Vector3 position = objects[i].transform.position;
			sw.WriteLine(position.x.ToString());
			sw.WriteLine(position.z.ToString());
			NodeInfo nodeInfo = new NodeInfo();
			objects[i].GetComponent<NodeScript>().GetNodeInfo();
			sw.WriteLine(nodeInfo.movementAttribute.ToString());
			sw.WriteLine(nodeInfo.spawnType.ToString());
			sw.WriteLine(nodeInfo.message);
			sw.WriteLine(nodeInfo.music);
		}
		Debug.Log("saved file " + slotNum.ToString() );
		sw.Close();
	}

	public void Load(int slotNum) {
		StreamReader sr = new StreamReader(getPath(slotNum));
		AllObjects objectManager = new AllObjects();
		GameObject[] objects;
		objects = GameObject.FindGameObjectsWithTag("Node");
		for(int i = 0; i < objects.Length; i++) {
			Destroy(objects[i]);
		}
		string line;
		while((line = sr.ReadLine()) != null) {
			float xPos = float.Parse(line);
			float zPos = float.Parse(sr.ReadLine());
			MovementType movement = objectManager.parseMovementType(sr.ReadLine());
			SpawnType spawn = objectManager.parseSpawnType(sr.ReadLine());
			string message = sr.ReadLine();
			string chordName = sr.ReadLine();
			GameObject temp = (GameObject)Instantiate(Resources.Load("Prefabs/Node"),
				new Vector3(Mathf.Floor(xPos), 0, Mathf.Floor(zPos)), Quaternion.identity);
			temp.GetComponent<NodeScript>().Initialize(movement, spawn, message, chordName);
		}
		sr.Close();
		Debug.Log("loaded file " + slotNum.ToString());
	}

	string getPath(int slotNum) {
		return string.Concat(Application.persistentDataPath, "/arboriaSave", slotNum.ToString(), ".txt");
	}
}
