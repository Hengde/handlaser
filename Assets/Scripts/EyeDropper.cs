﻿using UnityEngine;
using System.Collections;

public class EyeDropper : MonoBehaviour
{

    bool active = false;
    bool holding = false;
    bool clickt = false;
    NodeInfo currentNodeSetting;

    float mintolerance = 30f;
    float clickLength = 0f;
    float clickTime = 1f;

    TouchHandler th;

    void Start()
    {
        th = GetComponent<TouchHandler>();
    }

    void Update()
    {
        if (Input.touchCount >= 0)
        {
            Vector3 pos = Input.touches[0].position;
            //////////////////////////////////////////////////////////////////////////
            if (Input.GetTouch(0).phase != TouchPhase.Ended)
            {
                if (Vector3.Distance(transform.position, pos) <= mintolerance)
                {
                    if (clickt == true)
                    {
                        clickLength += Time.deltaTime;
                        if (clickLength >= clickTime)
                        {
                            active = true;
                            th.eyedropping = true;
                            //signal that we're in pickup mode

                        }
                    }
                    else
                    {
                        clickt = true;
                    }
                }
                else {
                    clickt = false;
                    clickLength = 0;
                }
            }
            //////////////////////////////////////////////////////////////////////////
            if (Input.GetTouch(0).phase == TouchPhase.Ended)
            {
                if (Vector3.Distance(transform.position, pos) >= mintolerance&& active == true)
                {
                        RaycastHit hit;
                        var ray = Camera.main.ScreenPointToRay(pos);
                        if (Physics.Raycast(ray, out hit) && hit.collider.gameObject.name != "Node")
                        {
                            currentNodeSetting = hit.collider.gameObject.GetComponent<NodeScript>().GetNodeInfo();
                            th.setNode(currentNodeSetting);
                            //change sprite somehow to reflect the new node    
                        }
                        active = false;
                    th.eyedropping = false;
                }
                else
                {
                    th.setNode(currentNodeSetting);
                    clickt = false;
                    clickLength = 0;
                }
                }
            }
        }
    }


