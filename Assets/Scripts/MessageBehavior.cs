﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MessageBehavior : MonoBehaviour {

	public Camera playerCamera;
	public bool goal;
    public string myText = "Welcome to Arboria!";
	public GameObject messageObject;
	// Use this for initialization
	void Start () {
		
	}

	IEnumerator endGame() {
		yield return new WaitForSeconds (2f);
		NodeScript.messageCanvas.transform.GetChild(0).gameObject.SetActive(false);
		GameObject.FindObjectOfType<TouchHandler>().switchMode();
	}



	void OnTriggerEnter(Collider other) {
		if(other.name.Contains("Player") && goal){
			NodeScript.messageCanvas.gameObject.SetActive(true);
			NodeScript.messageCanvas.transform.GetChild(0).gameObject.SetActive(true);
			NodeScript.messageCanvas.transform.GetChild(0).GetChild(1).GetComponent<Text>().text = "Nice work! You completed the game!";
			StartCoroutine("endGame");
		}
	}

	void OnTriggerExit(Collider other) {
		if(other.name.Contains("Player") && goal){
			NodeScript.messageCanvas.transform.GetChild(0).gameObject.SetActive(false);
			//mcanvas.transform.GetChild(0).GetChild(0).GetChild(1).GetComponent<Text>().text = message;
		}
	}



}
