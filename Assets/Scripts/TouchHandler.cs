﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

public class TouchHandler : MonoBehaviour {

	public Vector3 playerStartPos;
	public Vector3 playerStartRot;
	public Vector3 editorStartPos;
	public Vector3 editorStartRot;
	public GameObject Ground;
	public Camera playerCam;
	public Camera editorCam;
	public GameObject UIBlockPlane;
	private CharacterMovement movementManager;
	delegate void UpdateFunction();
	private UpdateFunction currentModeUpdate;
    //The entire Editor UI goes in here for easy enabling/disabling
    public GameObject editorUI;
	private GameObject startPoint;
    public string currentText = "TouchHandler handles the touches but does not touch the handles";
    public int currentSound = 0;
	private Dictionary<Vector2, GameObject> gridMembers;
    public bool eyedropping = false;

	public float cameraShiftSpeed = .1f;

	public float rotationTouchZoneSize = 50f;
	public float movementTouchZoneSize = 50f;

	public PlacementType PlacementMode;

    //maybe use this instead of placement mode
    public TileButton currentTB;

	// Use this for initialization
	[SerializeField]
	public NodeInfo currentNodeSettings;

	void Awake(){
		Application.targetFrameRate = 60;
		currentModeUpdate = playerModeUpdate;
		Screen.orientation = ScreenOrientation.LandscapeRight;
	}

	void Start () {
		currentNodeSettings = new NodeInfo();
		currentNodeSettings.message ="";
		GameObject.Find("RaycastPlane").gameObject.GetComponent<Renderer>().enabled = false;
		playerCam.enabled = true;
		editorCam.enabled = false;
		startPoint = GameObject.Find("StartPoint");
		playerStartPos = transform.position;

		movementManager = GetComponent<CharacterMovement>();
		gridMembers = new Dictionary<Vector2, GameObject>();
		foreach(GameObject node in GameObject.FindGameObjectsWithTag("Node")) {
			gridMembers.Add(new Vector2(Mathf.Floor(node.transform.position.x), 
										Mathf.Floor(node.transform.position.z)),
																		node);
		}
		switchMode();
	}
	
	// Update is called once per frame
	void Update () {
		if(currentModeUpdate !=null){
			currentModeUpdate();
		} else {
			currentModeUpdate = playerModeUpdate;
		}
//		if(Input.touchCount > 0){
//			if(Input.GetTouch(0).phase == TouchPhase.Moved){
////				Debug.Log(Input.GetTouch(0).position.ToString());
//			}
//		}
	}

	public void setNode(NodeInfo nodeInfo){
		currentNodeSettings = nodeInfo;
	}

	void playerModeUpdate() {
		if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved){
			// Get movement of the finger since last frame
			Vector2 touchDeltaPosition = Input.GetTouch(0).deltaPosition;

			// Move object across XY plane
			if(Mathf.Abs(touchDeltaPosition.x) < Mathf.Abs(touchDeltaPosition.y)){
				movementManager.move(touchDeltaPosition.y > 0 ? false : true);
			}
			// Rotate player
			else {
				movementManager.rotateDegrees(touchDeltaPosition.x > 0 ? 1.0f : -1.0f);
			}
		}
		else if(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Stationary) {
			Vector2 touchPosition = Input.GetTouch(0).position;
			if(touchPosition.x < rotationTouchZoneSize) {
				movementManager.rotateDegrees(-1.0f);
			}
			else if(touchPosition.x > Screen.width - rotationTouchZoneSize) {
				movementManager.rotateDegrees(1.0f);
			}
			if(touchPosition.y < movementTouchZoneSize) {
				movementManager.move(true);
			}
			else if(touchPosition.y > Screen.height - movementTouchZoneSize){
				movementManager.move(false);
			}
		}
	}

	void editorModeUpdate() {
			if (Input.touchCount > 1 && Input.GetTouch(1).phase == TouchPhase.Moved){
				// Get movement of the finger since last frame
				Vector2 touchDeltaPosition = Input.GetTouch(0).deltaPosition;
				// Move object across XY plane
				if(Mathf.Abs(touchDeltaPosition.x) < Mathf.Abs(touchDeltaPosition.y)){
					if(touchDeltaPosition.y >0){
						editorCam.transform.ShiftZ(-1*cameraShiftSpeed);
					} else {
						editorCam.transform.ShiftZ(cameraShiftSpeed);
					}
				} else {
					if(touchDeltaPosition.x >0){
						editorCam.transform.ShiftX(-1*cameraShiftSpeed);
					} else {
						editorCam.transform.ShiftX(cameraShiftSpeed);
					}
				}
			}

		 if(Input.touchCount == 1 && eyedropping == false){
			RaycastHit hit;
			var ray = Camera.main.ScreenPointToRay(Input.touches[0].position);
			if(Physics.Raycast(ray, out hit) && hit.collider.gameObject.tag != "Node" && hit.collider.name=="RaycastPlane") {
				Vector2 coordinates = new Vector2(Mathf.Floor(hit.point.x), Mathf.Floor(hit.point.z));
				if(!gridMembers.ContainsKey(coordinates)) {
					GameObject newNode = (GameObject)Instantiate(Resources.Load("Prefabs/Node"), new Vector3(Mathf.Floor(hit.point.x), -3.6f, Mathf.Floor(hit.point.z)), Quaternion.identity);
//					Debug.Log(name+"MSG?"+currentNodeSettings.message);
					newNode.GetComponent<NodeScript>().Initialize(currentNodeSettings.movementAttribute, currentNodeSettings.spawnType, currentNodeSettings.message, currentNodeSettings.music);
					gridMembers.Add(coordinates, newNode);
				}
			} else if(Physics.Raycast(ray, out hit) && hit.collider.gameObject.tag == "Node"){
				NodeScript myNode = hit.collider.gameObject.GetComponent<NodeScript>();
				myNode.Initialize(currentNodeSettings.movementAttribute, currentNodeSettings.spawnType, currentNodeSettings.message, currentNodeSettings.music);
			}
		}
	}

	public void switchMode() {
		if(currentModeUpdate == playerModeUpdate){
			currentModeUpdate = editorModeUpdate;

			startPoint.SetActive(true);
			startPoint.transform.SetY(200);
			UIBlockPlane.SetActive(true);

            editorUI.SetActive(true);
			GameObject.Find("RaycastPlane").gameObject.GetComponent<Renderer>().enabled = true;
			foreach(GameObject node in GameObject.FindGameObjectsWithTag("Node")) {
				node.GetComponent<NodeScript>().SwitchMode();
			}
			movementManager.resetPosition(startPoint.transform.position);
		}
		else {
			GameObject[] prints = GameObject.FindGameObjectsWithTag("Footprints");
			for(int i=prints.Length- 1; i >= 0; i--)
			{
				Destroy(prints[i]);
			}
			if(startPoint == null) {
				startPoint = GameObject.FindGameObjectWithTag("StartPoint");
			}
			GameObject.Find("RaycastPlane").gameObject.GetComponent<Renderer>().enabled = false;
			GameObject[] nodes = GameObject.FindGameObjectsWithTag("Node");
			foreach(GameObject node in nodes){
				node.GetComponent<NodeScript>().SwitchMode();
				if(node.GetComponent<NodeScript>().objectAttribute == SpawnType.StartPoint){
					startPoint.transform.position = new Vector3(node.transform.position.x, -2.12f, node.transform.position.z);
				}
			}
			currentModeUpdate = playerModeUpdate;
			playerStartPos = startPoint.transform.position;
			GameObject.Find("Player").transform.position = playerStartPos;
			startPoint.SetActive(false);
			UIBlockPlane.SetActive(false);
            editorUI.SetActive(false);
        }
		playerCam.enabled = !playerCam.enabled;
		editorCam.enabled = !editorCam.enabled;
	}
}
