﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public enum MovementType {Normal, Ice, Wall, Ignore};

public enum SpawnType {None, StartPoint, Goal, Key, Lock, Ignore};

[System.Serializable]
public struct NodeInfo {
	public MovementType movementAttribute;
	public SpawnType spawnType;
	public string message;
	public string music;
}

public class NodeScript : MonoBehaviour {

	public MovementType movementAttribute;
	public SpawnType objectAttribute;
	public string message;
	public AudioSource music;

	delegate void MovementEnterFunction(Collider other);
	delegate void MovementExitFunction(Collider other);
	delegate void SpawnEnterFunction(Collider other);
	delegate void SpawnExitFunction(Collider other);

	private MovementEnterFunction movementEnterFunction;
	private MovementExitFunction movementExitFunction;
	private SpawnEnterFunction spawnEnterFunction;
	private SpawnExitFunction spawnExitFunction;

	private bool editorMode = true;
	private bool initialized = false;
	private GameObject spawnedObject;
	private GameObject movementTile;
	private GameObject messageFlowers;
	private GameObject musicFlowers;

	public SpriteRenderer DisplayBG;
	public SpriteRenderer DisplaySpawnType;
	public SpriteRenderer DisplayMessage;
	public SpriteRenderer DisplayMusic;

	public bool isStartNode = false;

	public static GameObject messageCanvas;


	// Use this for initialization
	void Awake () {
		if(messageCanvas ==null){
			messageCanvas = GameObject.Find("MessageCanvas");
			messageCanvas.SetActive(false);
		}

		editorMode = !isStartNode;
		if(!initialized){
			music = GetComponent<AudioSource>();
			music.clip = null;
			movementEnterFunction = emptyFunction;
			movementExitFunction = emptyFunction;
			spawnEnterFunction = emptyFunction;
			spawnExitFunction = emptyFunction;
			Initialize(movementAttribute, objectAttribute, "", "");
			initialized = true;
		}
	}
	
	// Update is called once per frame
	void Update () {
	}

	public void SwitchMode() {
		editorMode = !editorMode;
		if(!editorMode) {
			SpawnObject();
			SpawnMovementTile();
			SpawnMessageFlowers();
			SpawnMusicFlowers();
			surroundWithWalls();
		}
		else {
			destroyFlowers();
			if(spawnedObject != null) {
				Destroy(spawnedObject);
			}
			if(movementTile != null) {
				Destroy(movementTile);
			}
			if(messageFlowers != null) {
				Destroy(messageFlowers);
			}
			if(musicFlowers != null) {
				Destroy(musicFlowers);
			}
			destroySurroundingWalls();
		}

	}

	public void Initialize(MovementType movementType, SpawnType spawnType, string msg, string clip) {
		if(movementType!=MovementType.Ignore){
			setMovementType(movementType);
		}
		if(movementType == MovementType.Wall){
			setMusic("");
			setMessage("");
			DisplayMusic.gameObject.SetActive(false);
			DisplayMessage.gameObject.SetActive(false);
		}
		if(spawnType != SpawnType.Ignore){
			setObjectAttribute(spawnType);
		}
		if(msg != "" && msg !=" "){
			DisplayMessage.gameObject.SetActive(true);
			setMessage(msg);
		} 

		if(clip != "" && clip !=" "){
			setMusic(clip);
			if(music.clip!=null){
				DisplayMusic.gameObject.SetActive(true);
			}
		} 
		initialized = true;
	}

	public NodeInfo GetNodeInfo() {
		NodeInfo returnData = new NodeInfo();
		returnData.movementAttribute = movementAttribute;
		returnData.spawnType = objectAttribute;
		returnData.message = message;
		returnData.music = music.clip != null ? music.clip.name : "";

		return returnData;
	}

	public void setMovementType(MovementType movementType) {
		movementAttribute = movementType;
		switch(movementAttribute) {
		case MovementType.Ice:
			movementEnterFunction = iceMovementEnter;
			movementExitFunction = iceMovementExit;
			DisplayBG.color = new Color(.14f,.8f,1f);
			break;
		case MovementType.Wall:
			DisplayBG.color = new Color(.34f,.56f,.21f);
			break;
		default:
			movementEnterFunction = emptyFunction;
			movementExitFunction = emptyFunction;
			DisplayBG.color = new Color(.67f,.52f,.22f);
			break;
		}
	}

	public void setObjectAttribute(SpawnType spawnType) {
		objectAttribute = spawnType;
		switch(spawnType){
		case SpawnType.None:
			DisplaySpawnType.gameObject.SetActive(false);
			spawnEnterFunction = emptyFunction;
			spawnExitFunction = emptyFunction;
			break;
		case SpawnType.Goal:
			DisplaySpawnType.gameObject.SetActive(true);
			DisplaySpawnType.sprite = Resources.Load<Sprite>("Images/UI/2Dicon/goal");

			foreach(GameObject node in GameObject.FindGameObjectsWithTag("Node")) {
				if(node != gameObject && node.GetComponent<NodeScript>().objectAttribute == SpawnType.Goal) {
					node.GetComponent<NodeScript>().unsetSpawn();
					break;
				}
			}
			break;
		case SpawnType.Key:
			DisplaySpawnType.gameObject.SetActive(true);
			DisplaySpawnType.sprite = Resources.Load<Sprite>("Images/UI/2Dicon/key");
			break;
		case SpawnType.Lock:
			DisplaySpawnType.gameObject.SetActive(true);
			DisplaySpawnType.sprite = Resources.Load<Sprite>("Images/UI/2Dicon/lock");
			break;
		case SpawnType.StartPoint:
			DisplaySpawnType.gameObject.SetActive(true);
			DisplaySpawnType.sprite = Resources.Load<Sprite>("Images/UI/2Dicon/spawn") as Sprite;
			foreach(GameObject node in GameObject.FindGameObjectsWithTag("Node")) {
				if(node != gameObject && node.GetComponent<NodeScript>().objectAttribute == SpawnType.StartPoint) {
					node.GetComponent<NodeScript>().unsetSpawn();
					break;
				}
			}
			break;
		default:
			spawnEnterFunction = emptyFunction;
			spawnExitFunction = emptyFunction;
			break;
		}
	}

	public void setMessage(string newMessage) {
		message = newMessage;
	}

	public void setMusic(string clip) {
		if(clip != "") {
		music.clip = Resources.Load<AudioClip>(string.Concat("Sounds/", clip));
		}
	}

	public void unsetSpawn(){
		if(music.clip != null){
			Initialize(movementAttribute, SpawnType.None, message, music.clip.name);
		} else {
			Initialize(movementAttribute, SpawnType.None, message, "");
		}
	}

	void OnTriggerEnter(Collider other) {
		if(other.name.Contains("Player")){
			movementEnterFunction(other);
			spawnEnterFunction(other);
			if(message != "") {
				messageCanvas.gameObject.SetActive(true);
				messageCanvas.transform.GetChild(0).gameObject.SetActive(true);
				messageCanvas.transform.GetChild(0).GetChild(1).GetComponent<Text>().text = message;
			}
			if(music.clip != null) {
				music.Play();
			}
		}
	}

	void OnTriggerExit(Collider other) {
		if(other.name.Contains("Player")){
			movementExitFunction(other);
			spawnExitFunction(other);
			if(message != "") {
				
				messageCanvas.transform.GetChild(0).gameObject.SetActive(false);
				//mcanvas.transform.GetChild(0).GetChild(0).GetChild(1).GetComponent<Text>().text = message;
			}
		}
	}

	private void emptyFunction(Collider other) {
		return;
	}

	private void iceMovementEnter(Collider other) {
		other.gameObject.GetComponent<CharacterMovement>().startSliding(gameObject);
	}

	private void iceMovementExit(Collider other) {
		other.gameObject.GetComponent<CharacterMovement>().stopSliding(gameObject);
	}

	bool checkForFloor(Ray theRay) {
		RaycastHit hit;
		for(int i = 0; i < 5; i++) {
			if(Physics.Raycast(theRay, out hit, 1f)) {
				return true;
			}
		}
		return false;
	}

	public void surroundWithWalls() {
		Ray northRay = new Ray(transform.position, transform.forward);
		Ray southRay = new Ray(transform.position, -transform.forward);
		Ray eastRay = new Ray(transform.position, transform.right);
		Ray westRay = new Ray(transform.position, -transform.right);

		GameObject temp;
		float xCorrection = gameObject.transform.localScale.x != 1 ? gameObject.transform.localScale.x / 2 : 1;
		float zCorrection = gameObject.transform.localScale.z != 1 ? gameObject.transform.localScale.z / 2 : 1;
		if(!checkForFloor(northRay)) {
			temp = (GameObject)Instantiate(Resources.Load("Prefabs/Wall"), transform.position + new Vector3 (0f, 1.0f, zCorrection), Quaternion.identity);
			temp.transform.localScale = new Vector3 (gameObject.transform.localScale.x, 2, 1);
			temp.transform.parent = gameObject.transform;
		}
		if(!checkForFloor(southRay)) {
			temp = (GameObject)Instantiate(Resources.Load("Prefabs/Wall"), transform.position + new Vector3 (0f, 1.0f, -zCorrection), Quaternion.identity);
			temp.transform.localScale = new Vector3 (gameObject.transform.localScale.x, 2, 1);
			temp.transform.parent = gameObject.transform;
		}
		if(!checkForFloor(eastRay)) {
			temp = (GameObject)Instantiate(Resources.Load("Prefabs/Wall"), transform.position + new Vector3 (xCorrection, 1.0f, 0f), Quaternion.identity);
			temp.transform.localScale = new Vector3 (1, 2, gameObject.transform.localScale.z);
			temp.transform.parent = gameObject.transform;
		}
		if(!checkForFloor(westRay)) {
			temp = (GameObject)Instantiate(Resources.Load("Prefabs/Wall"), transform.position + new Vector3 (-xCorrection, 1.0f, 0f), Quaternion.identity);
			temp.transform.localScale = new Vector3 (1, 2, gameObject.transform.localScale.z);
			temp.transform.parent = gameObject.transform;
		}
	}

	public void destroySurroundingWalls() {
		foreach (Transform child in gameObject.transform) {
			if(child.name.Contains("Wall")){
				GameObject.Destroy(child.gameObject);
			}
		}
	}

	void SpawnObject() {
		switch(objectAttribute) {
		case SpawnType.Goal: 
			spawnedObject = (GameObject)Instantiate(Resources.Load("Prefabs/Goal"), new Vector3(Mathf.Floor(transform.position.x), -2.5f, Mathf.Floor(transform.position.z)), Quaternion.identity);
			break;
		case SpawnType.Key:
			spawnedObject = (GameObject)Instantiate(Resources.Load("Prefabs/Key"), new Vector3(Mathf.Floor(transform.position.x), -2.5f, Mathf.Floor(transform.position.z)), Quaternion.identity);
			break;
		case SpawnType.Lock:
			spawnedObject = (GameObject)Instantiate(Resources.Load("Prefabs/Lock"), new Vector3(Mathf.Floor(transform.position.x), -2.5f, Mathf.Floor(transform.position.z)), Quaternion.identity);
			break;
		case SpawnType.StartPoint:
			spawnedObject = (GameObject)Instantiate(Resources.Load("Prefabs/StartPoint"), new Vector3(Mathf.Floor(transform.position.x), -2.5f, Mathf.Floor(transform.position.z)), Quaternion.identity);
			break;
		default:
			break;
		}
	}

	void SpawnMovementTile() {
		switch(movementAttribute) {
		case MovementType.Ice:
			movementTile = (GameObject)Instantiate(Resources.Load("Prefabs/Ice"), new Vector3(Mathf.Floor(transform.position.x), -3.1f, Mathf.Floor(transform.position.z)), Quaternion.identity);
			break;
		case MovementType.Wall:
			movementTile = (GameObject)Instantiate(Resources.Load("Prefabs/Wall"), new Vector3(Mathf.Floor(transform.position.x), -2.6f, Mathf.Floor(transform.position.z)), Quaternion.identity);
			break;
		default:
			break;
		}
	}
		
	void SpawnMessageFlowers() {
		if(message!=""){
			messageFlowers = (GameObject)Instantiate(Resources.Load("Prefabs/Message"), new Vector3(Mathf.Floor(transform.position.x), -2.6f, Mathf.Floor(transform.position.z)), Quaternion.identity);
		}
	}

	void SpawnMusicFlowers(){
		if(music.clip != null){
			musicFlowers = (GameObject)Instantiate(Resources.Load("Prefabs/Music"), new Vector3(Mathf.Floor(transform.position.x), -3.1f, Mathf.Floor(transform.position.z)), Quaternion.identity);
		}
	}

	void destroyFlowers(){
		if(messageFlowers != null) {
			Destroy(spawnedObject);
		}
		if(musicFlowers != null) {
			Destroy(movementTile);
		}
	}
}
