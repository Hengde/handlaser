﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum PlacementType {Camera, Wall, Floor, Key, Lock, Ice, Message, AMajor, Goal, StartPoint};

public class AllObjects : ScriptableObject {

	public GameObject WallObject;
	public Vector3 StartPoint;
    TouchHandler touchHandler;
	// Use this for initialization
	void Start () {
        touchHandler = FindObjectOfType<TouchHandler>();
	}

	public GameObject createObject(PlacementType type, Vector3 position) {
		GameObject temp;
		Debug.Log("createObject "+type);
		switch(type) {
			case PlacementType.Wall: 
				// TODO: Make the y value programmatic
				temp = (GameObject)Instantiate(Resources.Load("Prefabs/Wall"), new Vector3(Mathf.Floor(position.x), -2.5f, Mathf.Floor(position.z)), Quaternion.identity);
				//objects.Add(temp);
				break;
			case PlacementType.Key:
				temp = (GameObject)Instantiate(Resources.Load("Prefabs/Key"), new Vector3(Mathf.Floor(position.x), -2.5f, Mathf.Floor(position.z)), Quaternion.identity);
				//objects.Add(temp);
				break;
			case PlacementType.Lock:
				temp = (GameObject)Instantiate(Resources.Load("Prefabs/Lock"), new Vector3(Mathf.Floor(position.x), -2.5f, Mathf.Floor(position.z)), Quaternion.identity);
				break;
			case PlacementType.Ice:
				temp = (GameObject)Instantiate(Resources.Load("Prefabs/Ice"), new Vector3(Mathf.Floor(position.x), -3.0f, Mathf.Floor(position.z)), Quaternion.identity);
				break;
			case PlacementType.Message:
				temp = (GameObject)Instantiate(Resources.Load("Prefabs/Message"), new Vector3(Mathf.Floor(position.x), -2.5f, Mathf.Floor(position.z)), Quaternion.identity);
                temp.GetComponent<MessageBehavior>().myText = touchHandler.currentText;
				break;
			case PlacementType.AMajor:
				temp = (GameObject)Instantiate(Resources.Load("Prefabs/AMajor"), new Vector3(Mathf.Floor(position.x), -3.05f, Mathf.Floor(position.z)), Quaternion.identity);
                //this is where we'd set the sound type, using the int th.currentSound
				break;
			case PlacementType.Goal:
				temp = (GameObject)Instantiate(Resources.Load("Prefabs/Goal"), new Vector3(Mathf.Floor(position.x), -3.05f, Mathf.Floor(position.z)), Quaternion.identity);
				break;
			case PlacementType.StartPoint:
				temp = (GameObject)Instantiate(Resources.Load("Prefabs/StartPoint"), new Vector3(Mathf.Floor(position.x), 0, Mathf.Floor(position.z)), Quaternion.identity);
				temp.transform.position = new Vector3(Mathf.Floor(position.x), -2.5f, Mathf.Floor(position.z));
				Debug.Log("Placing player at");
				break;
			case PlacementType.Camera:
				temp = new GameObject();
				Debug.LogError("Someone is trying to place a camera");
				break;
			default:
				temp = new GameObject();
				break;
		}

		return temp;
	}

	public PlacementType parse(string type) {
		int numPlacementTypes = System.Enum.GetValues(typeof(PlacementType)).Length;
		for(PlacementType objectType = PlacementType.Wall; (int)objectType < numPlacementTypes; objectType++){
			if(objectType.ToString() == type) {
				return objectType;
				}
			}
		Debug.LogError("Invalid type sent");
		return PlacementType.Wall;
	}

	public MovementType parseMovementType(string movement) {
		MovementType returnVar = MovementType.Normal;
		switch(movement) {
			case "Ice":
				returnVar = MovementType.Ice;
				break;
			default:
				returnVar = MovementType.Normal;
				break;
		}
		return returnVar;
	}

	public SpawnType parseSpawnType(string spawn) {
		SpawnType returnVar = SpawnType.None;
		switch(spawn) {
			case "StartPoint":
				returnVar = SpawnType.StartPoint;
				break;
			case "Goal":
				returnVar = SpawnType.Goal;
				break;
			case "Key":
				returnVar = SpawnType.Key;
				break;
			case "Lock":
				returnVar = SpawnType.Lock;
				break;
			default:
				returnVar = SpawnType.None;
				break;
		}
		return returnVar;
	}

	public void placeFloorTile(Vector3 position) {
		Instantiate(Resources.Load("Prefabs/Floor"), new Vector3(Mathf.Floor(position.x), -3.5f, Mathf.Floor(position.z)), Quaternion.identity);
	}
}
