﻿using UnityEngine;
using System.Collections;

public class LockBehavior : MonoBehaviour {
	public GameObject innerGate;

	void OnTriggerEnter(Collider collision) {
		if(collision.name=="Player"){
			if(collision.gameObject.GetComponent<PlayerStats>().hasKey()) {
				Destroy(innerGate);
			}
		}
	}
}
