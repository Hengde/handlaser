﻿using UnityEngine;
using System.Collections;

public class IceScript : MonoBehaviour {

	void OnTriggerEnter(Collider other) {
		other.gameObject.GetComponent<CharacterMovement>().startSliding(gameObject);
	}

	void OnTriggerExit(Collider other) {
		other.gameObject.GetComponent<CharacterMovement>().stopSliding(gameObject);
	}
}
