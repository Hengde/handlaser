﻿using UnityEngine;
using System.Collections;

public class EditButton : MonoBehaviour {

    Vector3 activePos;

    // Use this for initialization
    void Start () {
		EndEdit();
        //transform.position = new Vector3(0, 0, 0);
	}

	// Update is called once per frame
	public void StartEdit () {
        //transform.position = activePos;
		gameObject.SetActive(true);
	}

    public void EndEdit() {
       // transform.position = new Vector3(0, 0, 0);
		gameObject.SetActive(false);
    }
}
