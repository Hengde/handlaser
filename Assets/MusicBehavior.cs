﻿using UnityEngine;
using System.Collections;

public class MusicBehavior : MonoBehaviour {

	private AudioSource chord;
	// Use this for initialization
	void Start () {
		chord = gameObject.GetComponent<AudioSource>();
		Debug.Log(chord);
	}
	
	// Update is called once per frame
	void OnTriggerEnter() {
		if(!chord.isPlaying) {
			chord.Play();
		}
	}
}
