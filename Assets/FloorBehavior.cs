﻿using UnityEngine;
using System.Collections;

public class FloorBehavior : MonoBehaviour {

	GameObject Wall;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	bool checkForFloor(Ray theRay) {
		RaycastHit hit;
			for(int i = 0; i < 5; i++) {
				if(Physics.Raycast(theRay, out hit, 1f)) {
				return true;
				}
			}
		return false;
	}

	public void surroundWithWalls() {
		Ray northRay = new Ray(transform.position, transform.forward);
		Ray southRay = new Ray(transform.position, -transform.forward);
		Ray eastRay = new Ray(transform.position, transform.right);
		Ray westRay = new Ray(transform.position, -transform.right);

		GameObject temp;
		float xCorrection = gameObject.transform.localScale.x != 1 ? gameObject.transform.localScale.x / 2 : 1;
		float zCorrection = gameObject.transform.localScale.z != 1 ? gameObject.transform.localScale.z / 2 : 1;
		if(!checkForFloor(northRay)) {
			temp = (GameObject)Instantiate(Resources.Load("Prefabs/Wall"), transform.position + new Vector3 (0f, 1.0f, zCorrection), Quaternion.identity);
			temp.transform.localScale = new Vector3 (gameObject.transform.localScale.x, 2, 1);
			temp.transform.parent = gameObject.transform;
		}
		if(!checkForFloor(southRay)) {
			temp = (GameObject)Instantiate(Resources.Load("Prefabs/Wall"), transform.position + new Vector3 (0f, 1.0f, -zCorrection), Quaternion.identity);
			temp.transform.localScale = new Vector3 (gameObject.transform.localScale.x, 2, 1);
			temp.transform.parent = gameObject.transform;
		}
		if(!checkForFloor(eastRay)) {
			temp = (GameObject)Instantiate(Resources.Load("Prefabs/Wall"), transform.position + new Vector3 (xCorrection, 1.0f, 0f), Quaternion.identity);
			temp.transform.localScale = new Vector3 (1, 2, gameObject.transform.localScale.z);
			temp.transform.parent = gameObject.transform;
		}
		if(!checkForFloor(westRay)) {
			temp = (GameObject)Instantiate(Resources.Load("Prefabs/Wall"), transform.position + new Vector3 (-xCorrection, 1.0f, 0f), Quaternion.identity);
			temp.transform.localScale = new Vector3 (1, 2, gameObject.transform.localScale.z);
			temp.transform.parent = gameObject.transform;
		}
	}

	public void destroySurroundingWalls() {
		//Debug.Log(gameObject.transform.childCount);
		foreach (Transform child in gameObject.transform) {
			GameObject.Destroy(child.gameObject);
		}
	}
}
