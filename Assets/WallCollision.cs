﻿using UnityEngine;
using System.Collections;

public class WallCollision : MonoBehaviour {
	
	// Update is called once per frame
	void OnTriggerEnter(Collider other) {
		if(other.gameObject.tag == "MainCamera") {
			other.gameObject.GetComponent<CharacterMovement>().stopSliding(null);
		}
	}

	void OnTriggerStay(Collider other) {
		if(other.gameObject.tag == "MainCamera") {
			other.gameObject.GetComponent<CharacterMovement>().stopSliding(null);
		}
	}
}
