﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EditPlayButton : MonoBehaviour {
    public Sprite playSprite;
    public Sprite editSprite;
    Image img;
    public TouchHandler th;
	bool editing = false;

	// Use this for initialization
	void Start () {
        img = this.GetComponent<Image>();
	}

    // Update is called once per frame
    public void changeState() {
		editing = !editing;
        th.switchMode();
        if (editing == true) {
            img.sprite = editSprite;
        }
        else { 
			img.sprite = playSprite;

  		  }

	}
}
